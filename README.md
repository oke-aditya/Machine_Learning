# Machine Learning Tutorials and Examples

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/oke-aditya/Machine_Learning/master)
[![HitCount](http://hits.dwyl.io/oke-aditya/Machine_Learning.svg)](http://hits.dwyl.io/oke-aditya/Machine_Learning)
![Dependabot compatibility score](https://dependabot-badges.githubapp.com/badges/compatibility_score?dependency-name=tensorflow&package-manager=pip&previous-version=2.0.0&new-version=2.1.0)
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/oke-aditya/Machine_Learning/pulls)


Tutorials, examples for Machine Learning.
- Credits to Learning Keras goes to Francois Chollet's book Deep Learning in Python.
- Also some of the notebooks are from sentdex www.pythonprogramming.net
- Most notebooks are scikit-learn implementations as given in documentation.
- The Kaggle submissions are my codes. They aren't take from any resource. 


- Binder link https://mybinder.org/v2/gh/oke-aditya/Machine_Learning/master
